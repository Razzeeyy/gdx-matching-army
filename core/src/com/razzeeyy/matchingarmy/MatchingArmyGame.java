package com.razzeeyy.matchingarmy;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ai.GdxAI;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.razzeeyy.matchingarmy.constants.World;
import com.razzeeyy.matchingarmy.screens.PreloadScreen;

public class MatchingArmyGame extends Game {
	private int GL_COVERAGE_BUFFER_BIT = 0;
	public Color background;

    public Stage world;
    public Stage ui;

    public AssetManager assets;

    public MessageDispatcher messages;

	@Override
	public void create () {
		GL_COVERAGE_BUFFER_BIT = Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0;

		background = Color.BLACK.cpy();

        world = new Stage(new FitViewport(World.WIDTH, World.HEIGHT));
        ui = new Stage(new ScreenViewport());

        Gdx.input.setInputProcessor(new InputMultiplexer(ui, world));

        assets = new AssetManager();

        messages = new MessageDispatcher();

        setScreen(new PreloadScreen(this));

    }

	@Override
	public void dispose () {
        super.dispose();
		background.set(Color.RED);

        world.dispose();
        ui.dispose();

        assets.dispose();

        messages.clear();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(background.r, background.g, background.b, background.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | GL_COVERAGE_BUFFER_BIT);

        GdxAI.getTimepiece().update(Gdx.graphics.getDeltaTime());
        messages.update();

        world.getViewport().apply();
        world.act();
        world.draw();

        ui.getViewport().apply();
        ui.act();
        ui.draw();

        super.render();
	}

    @Override
    public void resize(int width, int height) {
        ui.getViewport().update(width, height, true);
        world.getViewport().update(width, height, true);

        super.resize(width, height);
    }
}
