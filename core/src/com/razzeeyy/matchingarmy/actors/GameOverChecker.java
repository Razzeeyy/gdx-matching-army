package com.razzeeyy.matchingarmy.actors;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.razzeeyy.matchingarmy.constants.Events;

/**
 * Created by razze on 01.04.2017.
 */

public final class GameOverChecker extends Actor {
    private final MessageDispatcher messages;

    public GameOverChecker(MessageDispatcher messages) {
        this.messages = messages;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        final Stage world = getStage();

        boolean gameover = false;

        for (Actor actor : world.getActors()) {
            if (actor instanceof Tile && actor.isTouchable()) {
                Tile tile = (Tile) actor;

                if (tile.getY() < 0) {
                    gameover = true;
                    tile.setColor(Color.RED);
                }

                if (gameover) {
                    tile.setTouchable(Touchable.disabled);
                    tile.clear();
                }
            }
        }

        if (gameover) {
            remove(); //makes sure not to emit event multiple times by removing itself from execution
            messages.dispatchMessage(Events.GAME_OVER);
        }
    }
}
