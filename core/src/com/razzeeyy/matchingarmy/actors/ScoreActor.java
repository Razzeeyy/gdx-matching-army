package com.razzeeyy.matchingarmy.actors;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.razzeeyy.matchingarmy.constants.Events;

/**
 * Created by razze on 02.04.2017.
 */

public final class ScoreActor extends Label implements Telegraph {
    private int score = 0;
    private float timeLasted = 0;

    public ScoreActor(BitmapFont font) {
        super("0", new LabelStyle(font, Color.BLACK.cpy()));
    }

    public void hookListeners(MessageDispatcher messages) {
        messages.addListener(this, Events.MATCH);
    }

    public void unhookListeners(MessageDispatcher messages) {
        messages.removeListener(this, Events.MATCH);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        timeLasted += delta;

        setPosition(
                (getStage().getWidth()-getPrefWidth())*0.5f,
                getPrefHeight()*0.2f
        );
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        if (msg.message == Events.MATCH) {
            int size = 0;
            if (msg.extraInfo instanceof Integer){
                Integer matchSize = (Integer) msg.extraInfo;
                size = matchSize;
            }
            score += size * size + MathUtils.floor(timeLasted);
            setText(Integer.toString(score));
            return true;
        }
        return false;
    }

    public int getScore() {
        return score;
    }
}
