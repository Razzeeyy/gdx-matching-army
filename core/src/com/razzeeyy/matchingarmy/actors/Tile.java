package com.razzeeyy.matchingarmy.actors;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.razzeeyy.matchingarmy.actors.actions.PushCheckAction;
import com.razzeeyy.matchingarmy.constants.Events;

/**
 * Created by razze on 28.03.2017.
 */

public final class Tile extends Image {
    public final int type;
    private final MessageDispatcher messages;
    private final Array<Actor> visited = new Array<Actor>();
    private final Array<Tile> match = new Array<Tile>();

    private static final Color color1 = new Color(0.25f, 0.75f, 0, 1);
    private static final Color color2 = new Color(0.5f, 0, 0.5f, 1);
    private static final Color color3 = new Color(0, 0.25f, 0.75f, 1);

    public Tile(final MessageDispatcher messages, Texture texture, int type) {
        super(texture);

        this.messages = messages;

        setSize(1, 1);

        this.type = type;

        switch (type) {
            case 0:
                setColor(color1);
                break;
            case 1:
                setColor(color2);
                break;
            case 2:
                setColor(color3);
                break;
        }

        setTouchable(Touchable.enabled);

        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                visited.clear();
                match.clear();

                floodfill(null, null);

                if (match.size > 1) {
                    for (Tile tile : match) {
                        tile.setTouchable(Touchable.disabled);
                        tile.clear();
                        tile.addAction(Actions.sequence(Actions.fadeOut(0.2f), Actions.removeActor()));
                    }

                    messages.dispatchMessage(Events.MATCH, Integer.valueOf(match.size));
                }
            }
        });

        addAction(Actions.action(PushCheckAction.class));
    }

    public Tile tileUp() {
        return (Tile) getStage().hit(getX() + getWidth() * 0.5f, getY() + getHeight() * 1.1f, true);
    }

    public Tile tileDown() {
        return (Tile) getStage().hit(getX() + getWidth() * 0.5f, getY() - getHeight() * 0.1f, true);
    }

    public Tile tileLeftTop() {
        return (Tile) getStage().hit(getX() - getWidth() * 0.1f, getY() + getHeight() * 0.9f, true);
    }

    public Tile tileLeftBottom() {
        return (Tile) getStage().hit(getX() - getWidth() * 0.1f, getY() + getHeight() * 0.1f, true);
    }

    public Tile tileRightTop() {
        return (Tile) getStage().hit(getX() + getWidth() * 1.1f, getY() + getHeight() * 0.9f, true);
    }

    public Tile tileRightBottom() {
        return (Tile) getStage().hit(getX() + getWidth() * 1.1f, getY() + getHeight() * 0.1f, true);
    }

    private void floodfill(Tile tile, Tile check) {
        if (tile == null) {
            tile = (Tile) getStage().hit(getX() + 0.5f, getY() + 0.5f, true);
        }

        if (check == null) {
            check = tile;
        }

        if (!visited.contains(check, true)) {
            visited.add(check);
            if (tile != null && check != null && tile.type == check.type) {
                match.add(check);

                floodfill(tile, check.tileUp());
                floodfill(tile, check.tileDown());
                floodfill(tile, check.tileLeftTop());
                floodfill(tile, check.tileLeftBottom());
                floodfill(tile, check.tileRightTop());
                floodfill(tile, check.tileRightBottom());
            }
        }
    }
}
