package com.razzeeyy.matchingarmy.actors;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.razzeeyy.matchingarmy.constants.Assets;

/**
 * Created by razze on 01.04.2017.
 */

public final class WaveSpawner extends Actor {
    private final float INTERVAL = 1f;
    private final int TYPES = 3;
    private final MessageDispatcher messages;
    private final AssetManager assets;
    private float sinceLastWave = INTERVAL;

    public WaveSpawner(MessageDispatcher messages, AssetManager assets) {
        this.messages = messages;
        this.assets = assets;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        sinceLastWave += delta;

        if (sinceLastWave > INTERVAL) {
            sinceLastWave -= INTERVAL;

            final Stage world = getStage();

            for (int i = 0; i < world.getWidth(); i++) {
                final Tile tile = new Tile(messages, assets.get(Assets.TILE, Texture.class), MathUtils.random.nextInt(TYPES));
                tile.setPosition(i, world.getHeight());
                tile.addAction(Actions.moveBy(0, -1, INTERVAL));
                world.addActor(tile);
            }
        }
    }
}
