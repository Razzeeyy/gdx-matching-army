package com.razzeeyy.matchingarmy.actors.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.razzeeyy.matchingarmy.actors.Tile;

/**
 * Created by razze on 01.04.2017.
 */

public final class PushCheckAction extends Action {
    @Override
    public boolean act(float delta) {
        Tile tile = (Tile)getTarget();
        if(tile == null) {
            return true;
        }

        Actor candidate = tile.tileDown(); //collision candidate
        if (candidate != null) {
            final float distance = tile.getY() - candidate.getY();
            if (distance < candidate.getWidth()) { //if collided
                candidate.moveBy(0, distance - candidate.getWidth()); //push that tile down a bit
            }
        }

        return false;
    }
}
