package com.razzeeyy.matchingarmy.constants;

/**
 * Created by razzeeyy on 25.03.17.
 */

public abstract class Assets {
    public static final String BADLOGIC = "badlogic.jpg";
    public static final String PIXEL = "pixel.png";
    public static final String TILE = "pixel.png";
    public static final String FONT = "font.fnt";
    public static final String HISCORE = "hiscore.png";
    public static final String SCORE = "currentscore.png";
    public static final String MATCH = "match.wav";
    public static final String GAMEOVER = "gameover.wav";
    public static final String RESTART = "restart.wav";
    public static final String ICON_RESTART = "restart.png";
}
