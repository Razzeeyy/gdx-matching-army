package com.razzeeyy.matchingarmy.constants;

/**
 * Created by razzeeyy on 25.03.17.
 */

public abstract class Events {
    public static final int GAME_OVER = 1;
    public static final int MATCH = 2;
}
