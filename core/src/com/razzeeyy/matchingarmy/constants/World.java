package com.razzeeyy.matchingarmy.constants;

/**
 * Created by razzeeyy on 25.03.17.
 */

public abstract class World {
    public static final int WIDTH = 5;
    public static final int HEIGHT = 7;
    public static final int TILE_TYPES = 4;
}
