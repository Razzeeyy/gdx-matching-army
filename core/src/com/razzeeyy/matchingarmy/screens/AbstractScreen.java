package com.razzeeyy.matchingarmy.screens;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.razzeeyy.matchingarmy.MatchingArmyGame;

/**
 * Created by razzeeyy on 25.03.17.
 */

public abstract class AbstractScreen extends ScreenAdapter {
    protected MatchingArmyGame game;

    protected Color background;

    protected Stage world;
    protected Stage ui;

    protected AssetManager assets;

    protected MessageDispatcher messages;

    public AbstractScreen(MatchingArmyGame game) {
        this.game = game;

        background = game.background;

        world = game.world;
        ui = game.ui;

        assets = game.assets;

        messages = game.messages;
    }
}
