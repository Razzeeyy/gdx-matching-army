package com.razzeeyy.matchingarmy.screens;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.razzeeyy.matchingarmy.MatchingArmyGame;
import com.razzeeyy.matchingarmy.actors.GameOverChecker;
import com.razzeeyy.matchingarmy.actors.ScoreActor;
import com.razzeeyy.matchingarmy.actors.Tile;
import com.razzeeyy.matchingarmy.actors.WaveSpawner;
import com.razzeeyy.matchingarmy.constants.Assets;
import com.razzeeyy.matchingarmy.constants.Events;

/**
 * Created by razze on 26.03.2017.
 */

public final class GameScreen extends AbstractScreen implements Telegraph {
    private final WaveSpawner waveSpawner;
    private final GameOverChecker gameOverChecker;
    private final ScoreActor scoreActor;
    private final Sound gameoverSound;
    private final Sound matchSound;
    private boolean gameover;

    public GameScreen(MatchingArmyGame game) {
        super(game);
        waveSpawner = new WaveSpawner(messages, assets);
        gameOverChecker = new GameOverChecker(messages);
        scoreActor = new ScoreActor(assets.get(Assets.FONT, BitmapFont.class));

        gameoverSound = assets.get(Assets.GAMEOVER, Sound.class);
        matchSound = assets.get(Assets.MATCH, Sound.class);
    }

    @Override
    public void show() {
        super.show();

        background.set(Color.WHITE);

        messages.addListener(this, Events.GAME_OVER);
        messages.addListener(this, Events.MATCH);

        world.addActor(waveSpawner);
        world.addActor(gameOverChecker);

        ui.addActor(scoreActor);
        scoreActor.hookListeners(messages);

        //create deadzone (gameover zone) indication
        for(int i=0; i<world.getWidth(); i++) {
            final Tile tile = new Tile(messages, assets.get(Assets.TILE, Texture.class), 0);
            tile.setPosition(i, 0);
            tile.setTouchable(Touchable.disabled);
            tile.setColor(Color.RED.cpy());
            tile.addAction(Actions.alpha(0.25f, 0.5f));
            world.addActor(tile);
        }
    }

    @Override
    public void hide() {
        super.hide();

        messages.removeListener(this, Events.GAME_OVER);
        messages.removeListener(this, Events.MATCH);

        waveSpawner.remove();
        gameOverChecker.remove();

        scoreActor.remove();
        scoreActor.unhookListeners(messages);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if (gameover) switchToGameOver();
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        switch (msg.message) {
            case Events.GAME_OVER:
                gameover = true;
                gameoverSound.play();
                return true;
            case Events.MATCH:
                matchSound.play(0.5f);
                return true;
            default:
                return false;
        }
    }

    public void switchToGameOver() {
        game.setScreen(new OverScreen(game, scoreActor.getScore()));
        this.dispose();
    }
}
