package com.razzeeyy.matchingarmy.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.razzeeyy.matchingarmy.MatchingArmyGame;
import com.razzeeyy.matchingarmy.actors.Tile;
import com.razzeeyy.matchingarmy.constants.Assets;

/**
 * Created by razzeeyy on 01.04.17.
 */

public final class OverScreen extends AbstractScreen {
    private static final String SAVED_HISCORE = "hiscore";
    private final int currentScore;
    private final int highScore;
    private final Sound restartSound;
    private Image restart;
    private Table scores;

    private Action restartAction = new Action() {
        @Override
        public boolean act(float delta) {
            switchToGame();
            return true;
        }
    };

    public OverScreen(MatchingArmyGame game, int score) {
        super(game);

        currentScore = score;

        Preferences saves = Gdx.app.getPreferences(MatchingArmyGame.class.getName());
        highScore = saves.getInteger(SAVED_HISCORE, 0);

        if (currentScore > highScore) {
            saves.putInteger(SAVED_HISCORE, currentScore);
            saves.flush();
        }

        restartSound = assets.get(Assets.RESTART, Sound.class);
    }

    @Override
    public void show() {
        super.show();

        scores = new Table();
        scores.setFillParent(true);
        ui.addActor(scores);

        scores.getColor().a = 0;
        scores.addAction(Actions.fadeIn(1f));


        final BitmapFont font = assets.get(Assets.FONT, BitmapFont.class);
        final float fontSize = font.getLineHeight();

        if (currentScore > highScore) {
            scores.add(new Image(assets.get(Assets.HISCORE, Texture.class)))
                    .width(fontSize)
                    .height(fontSize)
                    .padBottom(fontSize / 4);
            scores.row();
        }

        scores.add(new Label(Integer.toString(currentScore), new Label.LabelStyle(font, Color.BLACK.cpy())));


        restart = new Image(assets.get(Assets.ICON_RESTART, Texture.class));
        scores.row();
        scores.add(restart).width(fontSize).height(fontSize).padTop(fontSize);

        restart.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                restart.setTouchable(Touchable.disabled);
                restart.addAction(
                        Actions.sequence(
                                Actions.parallel(
                                        Actions.moveBy(0, fontSize, 0.5f),
                                        Actions.fadeOut(0.25f)
                                ),
                                Actions.removeActor()
                        )
                );
                restartSound.play();

                final float animationTime = 1f;

                for (Actor actor : world.getActors()) {
                    if (actor instanceof Tile) {
                        actor.addAction(
                                Actions.sequence(
                                        Actions.moveBy(0, world.getHeight() + 0.01f, animationTime)
                                )
                        );
                    }
                }

                final Actor restarter = new Actor(); //will restart after all tiles assumed to be tweened outta screen
                restarter.addAction(Actions.delay(animationTime + 0.3f, restartAction));
                world.addActor(restarter);
            }
        });
    }

    @Override
    public void hide() {
        super.hide();
        restart.remove();
        scores.remove();
    }

    public void switchToGame() {
        this.dispose();
        world.clear();
        game.setScreen(new GameScreen(game));
    }
}
