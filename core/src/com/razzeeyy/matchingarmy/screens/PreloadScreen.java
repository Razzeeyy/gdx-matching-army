package com.razzeeyy.matchingarmy.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.razzeeyy.matchingarmy.MatchingArmyGame;
import com.razzeeyy.matchingarmy.constants.Assets;

/**
 * Created by razzeeyy on 25.03.17.
 */

public final class PreloadScreen extends AbstractScreen {
    public PreloadScreen(MatchingArmyGame game) {
        super(game);
    }

    @Override
    public void show() {
        super.show();

        background.set(Color.BLACK);

        assets.load(Assets.BADLOGIC, Texture.class);
        assets.load(Assets.PIXEL, Texture.class);
        assets.load(Assets.TILE, Texture.class);
        assets.load(Assets.FONT, BitmapFont.class);
        assets.load(Assets.HISCORE, Texture.class);
        assets.load(Assets.SCORE, Texture.class);
        assets.load(Assets.MATCH, Sound.class);
        assets.load(Assets.RESTART, Sound.class);
        assets.load(Assets.GAMEOVER, Sound.class);
        assets.load(Assets.ICON_RESTART, Texture.class);
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if(assets.update()) {
            bootstrap();
            switchToMenu();
        }
    }

    private void bootstrap() {
        BitmapFont font = assets.get(Assets.FONT, BitmapFont.class);
        font.getData().setScale(Gdx.graphics.getDensity()); //resolution independence for fonts
        //make fonts use linear filtering
        for (TextureRegion textureRegion : font.getRegions()) {
            textureRegion.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
    }

    private void switchToMenu() {
        game.setScreen(new GameScreen(game));
        this.dispose();
    }
}
