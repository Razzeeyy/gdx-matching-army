package com.razzeeyy.matchingarmy.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.razzeeyy.matchingarmy.MatchingArmyGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Marching Tiles";
		config.samples = 2;
		//TODO: desktop icons
		new LwjglApplication(new MatchingArmyGame(), config);
	}
}
